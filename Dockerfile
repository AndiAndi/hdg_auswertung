FROM golang:1.15-alpine as builder
WORKDIR /work
COPY . /work
RUN go build

FROM alpine
RUN apk update && apk add tzdata && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    echo "Europe/Berlin" > /etc/timezone && \
    rm -rf /var/cache/apk/*

COPY --from=builder /work/hdg_auswertung /usr/local/bin/hdg_auswertung

COPY cron_root /etc/crontabs/root
RUN chown root:root /etc/crontabs/root
CMD /usr/sbin/crond -f

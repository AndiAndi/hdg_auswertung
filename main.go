package main

import (
	"encoding/csv"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
	_ "time/tzdata"

	"sync"

	"net/textproto"

	// Read local file
	"path/filepath"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"

	ifclient "github.com/influxdata/influxdb1-client/v2"
)

func normalizeGerman(old string) string {
    return strings.Replace(old, ",", ".", 1)
}

func getHdgAttachments(ch chan string) {
    // Connect to server
    log.Println("Connecting to server...")
    c, err := client.DialTLS(os.Getenv("IMAP_SERVER")+":"+os.Getenv("IMAP_PORT"), nil)
    if err != nil {
        log.Fatal(err)
    }
    log.Println("Connected")

    // Don't forget to logout
    defer c.Logout()

    // Login
    if err := c.Login(os.Getenv("IMAP_USER"), os.Getenv("IMAP_PASSWORD")); err != nil {
        log.Fatal(err)
    }
    log.Println("Logged in")

    // Select INBOX
    _, err = c.Select("INBOX", false)
    if err != nil {
        log.Fatal(err)
    }

    // Set search criteria
    now := time.Now()
    envSearchTime, err := strconv.ParseInt(os.Getenv("SEARCH_TIME"), 0, 0)
    if err != nil {
        log.Fatal(err)
    }
    searchTime := now.Add(-1 * time.Duration(envSearchTime) * time.Hour)
    criteria := imap.NewSearchCriteria()
    criteria.Header = textproto.MIMEHeader{"Subject": {"Logfile"}}
    criteria.Since = searchTime
    ids, err := c.Search(criteria)
    if err != nil {
        log.Fatal(err)
    }
    log.Println("IDs found:", ids)

    // Process found mails
    if len(ids) > 0 {
        for selID := range ids {
            id := ids[selID]
            log.Println("Id: ", id)
            seqSet := new(imap.SeqSet)
            seqSet.AddNum(id)

            // Get the whole message body
            var section imap.BodySectionName
            items := []imap.FetchItem{section.FetchItem()}

            messages := make(chan *imap.Message, 1)
            go func() {
                if err := c.Fetch(seqSet, items, messages); err != nil {
                    log.Fatal(err)
                }
            }()

            msg := <-messages
            if msg == nil {
                log.Fatal("Server didn't returned message")
            }

            r := msg.GetBody(&section)
            if r == nil {
                log.Fatal("Server didn't returned message body")
            }

            // Create a new mail reader
            mr, err := mail.CreateReader(r)
            if err != nil {
                log.Fatal(err)
            }

            // Process each message's part
            for {
                p, err := mr.NextPart()
                if err == io.EOF {
                    break
                } else if err != nil {
                    log.Fatal(err)
                }

                switch h := p.Header.(type) {
                case *mail.AttachmentHeader:
                    // This is an attachment
                    filename, err := h.Filename()
                    if err != nil {
                        log.Fatal(err)
                    }
                    b, err := ioutil.ReadAll(p.Body)
                    if err != nil {
                        log.Fatal(err)
                    }

                    log.Println("Got attachment: ", filename)
                    // return attachment
                    ch <- string(b)
                }
                // TODO: define action/error when no attachment is found
            }
        }
    } else {
        log.Println("No mail found") // TODO: what if no mail was found?
    }
    close(ch)
}

func writeInfluxdb(data string) {
    defer wg.Done()

    // Make influxdb client
    dbaddr := os.Getenv("INFLUXDB_ADDRESS")
    log.Println("Establish connection to InfluxDB: ", dbaddr)
    idbc, err := ifclient.NewHTTPClient(ifclient.HTTPConfig{
        Addr: dbaddr,
    })
    if err != nil {
        log.Fatal(err)
    }
    defer idbc.Close()

    reader := csv.NewReader(strings.NewReader(data))
    reader.Comma = ';'

    // Create a new point batch
    bp, err := ifclient.NewBatchPoints(ifclient.BatchPointsConfig{
        Database:  os.Getenv("INFLUXDB_DB"),
        Precision: "s",
    })
    if err != nil {
        log.Fatal(err)
    }

    header, err := reader.Read()
    if err != nil {
        log.Fatal(err)
    }
    log.Println("parse CSV content")
    for {
        record, err := reader.Read()
        if err == io.EOF {
            break
        }
        if err != nil {
            log.Fatal(err)
        }
        loc, err := time.LoadLocation("Europe/Berlin")
        if err != nil {
            panic(err)
        }
        timeval, err := time.ParseInLocation("02.01.2006T15:04:05", record[0]+"T"+record[1], loc)
        if err != nil {
            log.Fatal(err)
        }
        fields := make(map[string]interface{})
        for i := 2; i < len(record)-1; i++ {
            if record[i] != "" {
                fields[header[i]], err = strconv.ParseFloat(normalizeGerman(record[i]), 64)
                if err != nil {
                    panic(err)
                }
            }
        }
        tags := map[string]string{}
        pt, err := ifclient.NewPoint("hdg", tags, fields, timeval)
        if err != nil {
            log.Println("Error ifclient.NewPoint: ", err.Error())
        }
        bp.AddPoint(pt)

    }
    log.Println("Write points to influx")
    err = idbc.Write(bp)
    if err != nil {
        log.Fatal(err)
    }
}

func readLocalFiles(c chan string) {
    dir, err := os.Getwd()
    if err != nil {
        log.Fatal(err)
    }
    matches, err := filepath.Glob(dir + string(os.PathSeparator) + "*.CSV")
    if err != nil {
        log.Fatal(err)
    }

    if len(matches) != 0 {
        log.Println("Found : ", matches)
        for i := range matches {
            _, filename := filepath.Split(matches[i])
            log.Println("Read: ", filename)
            b, err := ioutil.ReadFile(matches[i])
            if err != nil {
                log.Fatal(err)
            }
            c <- string(b)
        }
    } else {
        log.Println("No files found")
    }
    close(c)
}

var wg sync.WaitGroup

func main() {
    startTime := time.Now()

    c := make(chan string)

    switch source := os.Getenv("CSV_SOURCE"); source {
    case "IMAP":
        go getHdgAttachments(c)
    case "LOCAL":
        go readLocalFiles(c)
    default:
        log.Println("Use defaut CSV source")
        go getHdgAttachments(c)
    }

    // Create database if not exits
    idbc, err := ifclient.NewHTTPClient(ifclient.HTTPConfig{
        Addr: os.Getenv("INFLUXDB_ADDRESS"),
    })
    if err != nil {
        log.Fatal(err)
    }
    q := ifclient.NewQuery("SHOW DATABASES", "", "")
    response, err := idbc.Query(q)
    if err != nil {
        log.Fatal(err)
    }
    dbNotExists := true
    for _, element := range response.Results[0].Series[0].Values {
        if element[0] == os.Getenv("INFLUXDB_DB"){
            dbNotExists = false
        }
    }
    if dbNotExists{
        log.Println("Create DB "+os.Getenv("INFLUXDB_DB"))
        q = ifclient.NewQuery("CREATE DATABASE "+os.Getenv("INFLUXDB_DB"), "", "")
        response, err = idbc.Query(q)
        if err != nil {
            log.Fatal(err)
        }
    }
    idbc.Close()

    for i := range c {
        wg.Add(1)
        go writeInfluxdb(i)
    }

    wg.Wait()
    stopTime := time.Now()
    log.Println("Execution time: ", stopTime.Sub(startTime))
}

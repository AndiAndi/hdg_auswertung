# Beschreibung
Das Programm erlaubt es, CSV Dateien der Heizungssteuerung HDG Control einzulesen und in die Zeitreihendatenbank InfluxDB zu schreiben. Die Daten können anschließend beispielsweise mit Grafana visualisiert werden.  
Die CSV Dateien können entweder lokal abgelegt oder aus einem E-Mail Postfach per IMAP abgerufen werden.  
Das Programm wurde in Go geschrieben und ist somit plattformunabhängig.  

Die Konfiuguration erfolgt über Umgebungsvariablen. Die folgende Tabelle zeigt die zur Verfügung stehenden Optionen:  

| Name | Beschreibung |
| :-----     | :-------------- |
| IMAP_USER | Benutzername für das IMAP Konto |
| IMAP_PASSWORD | Passwort für das IMAP Konto |
| IMAP_SERVER | Adresse des IMAP Servers |
| IMAP_PORT | Port des IMAP Servers |
| SEARCH_TIME | Zeit in Stunden, wie weit E-Mails in der Vergangenheit gesucht werden. <br>Für den Wert 48, wird nach E-Mails gesucht, die in den letzten 48 Stunden im Postfach empfangen wurden |
| INFLUXDB_ADDRESS | Adresse der InfluxDB Instanz, z.B. `http://localhost:8086` |
| INFLUXDB_DB | Name der Datenbank die Verwendet werden soll. Z.B. `mydb`. <br>Ist die Datenbank nicht vorhanden, wird diese angelegt. |
| CSV_SOURCE | Bestimmt die Quelle, von der die CSV-Dateien gelesen werden. <br>`IMAP` für die Abfrage von einem IMAP-Konto, dafür muss ein gültiges IMAP-Konto angegeben werden. <br>`LOCAL` die CSV Dateien müssen lokal in dem Verzeichnis der Applikation abgelegt werden <br> Falls diese Umgebungsvariable nicht gesetzt ist, wird `IMAP` angenommen |

Am Einfachsten ist die Installation unter Linux mit de bereitgestellten `docker-compose.yml` Datei.  
Für alle anderen wird im Folgenden die Installation unter Windows grob beschrieben. 

# Installation unter Windows
Für die Installation unter Windows sind die folgenden Schritte notwendig.  
## Installation von InfluxDB 
Eine detailierte Beschreibung zur Installation gibt es hier: http://richardn.ca/2019/01/04/installing-influxdb-on-windows/  
Grob werden folgende Schritte benötigt: 
1. InfluxDB 1.x von https://portal.influxdata.com/downloads/ herunterladen und extrahieren
2. Folgende Einstellungen in der Datei influxdb.conf anpassen: 
   - [meta] dir
   - [data] dir 
   - [data] wal-dir
3. `influxd.exe` starten
4. Anschließend im Browser http://localhost:8086/query aufrufen. Hier sollte `"missing required parameter \"q\""` erscheinen  

## HDG Auswertung anpassen und starten
1. Das Programm aus dem letzen Release von https://gitlab.com/AndiAndi/hdg_auswertung/-/releases herunterladen und extrahieren 
2. `IMAP_USER`, `IMAP_PASSWORD`, `IMAP_SERVER`, `IMAP_PORT` und `SEARCH_TIME` in der Datei `env_settings.ps1` anpassen. 
   - Falls nicht das E-Mail Postfach durchsucht werden soll, kann die auskommentierte Zeile `$env:CSV_SOURCE = "LOCAL"` aktiviert werden. Die CSV-Dateien müssen dann im gleichen Verzeichnis wie die `main.exe` liegen. 
3. powershell öffnen
4. In den Ordner wechseln in dem die Dateien `env_settings.ps1` und `main.exe` liegen
5. Den Befehl `.\env_settings.ps1` ausführen 
   - Falls der Fehler `.\env_settings.ps1 : Die Datei "c:\xxx\env_settings.ps1" kann nicht geladen werden, da die Ausführung von Skripts auf diesem System deaktiviert ist.` erscheint, ist hier beschrieben, wie der Fehler umgangen werden kann: https://lycantec.com/wissen/scripting/powershell-skript-kann-nicht-geladen-werden/. Alternativ kann man den Inhalt der Datei `env_settings.ps1` in die Powershell einfügen. 
6. Den Befehl `.\main.exe` ausführen  

Anschließend sollten die CSV-Datein aus dem E-Mail Postfach heruntergeladen und in die InfluxDB geschrieben werden.  

Um neue Daten abzurufen, sind die Schritte 4-6 zu wiederholen.  

## Installation von Grafana

1. Grafana herunterladen und installieren/starten. Die Schritte hierzu werden hier beschrieben: https://grafana.com/docs/grafana/latest/installation/windows/
2. Den Getting started Guide von Grafana ausführen: https://grafana.com/docs/grafana/latest/getting-started/getting-started/. Anschließend sollte man auf der Oberfläche von Grafana landen. 
3. Zunächst muss InfluxDB als Quelle hinzugefügt werden. Hierzu geht man im linken Menü auf `Configuration - Data Sources` und anschließend auf `Add data source`. 
4. InfluxDB auswählen
5. Als URL `http://localhost:8086` und unter Datbase `mydb` eintragen und mit `Safe & Test` bestätigen. Anschließend sollte `Datasource is working` erscheinen 
6. Im linken Menü unter `Dashboards - Manage` kann jetzt ein neues Dashboard angelegt werden. 
7. Über Add Panel können jetzt Auswertungen und Visualisierungen angelegt werden. 
8. In folgendem Screenshot sieht man, wie man über eine Abfrage die Temperatur des Puffers darstellen kann: 
![add_panel.png](./help/add_panel.png)

Es kann auch ein fertiges Dashboard in Grafana importiert werden. Unter https://gitlab.com/AndiAndi/hdg_auswertung/-/blob/master/grafana-provisioning/dashboards/hdg.json ist ein Beispieldashboard zu sehen. Dieses kann über `Create - Import` aus dem linken Menü importiert werden. 

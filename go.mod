module gitlab.com/AndiAndi/hdg_auswertung

go 1.14

require (
	github.com/emersion/go-imap v1.0.5
	github.com/emersion/go-message v0.12.0
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
)
